package com.example.fr.backstack;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by fr on 20.08.14.
 */
public class LoggingActivity extends Activity {

    private String TAG = getClass().getName();

    private TextView textViewById;
    private Button buttonViewById1;
    private Button buttonViewById2;

    private Intent buttonIntent1;
    private Intent buttonIntent2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.v(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_layout);
        prepareUI();
    }

    private void prepareUI() {
        textViewById = (TextView) findViewById(R.id.text);
        buttonViewById1 = (Button) findViewById(R.id.button1);
        buttonViewById2 = (Button) findViewById(R.id.button2);
        if(null != buttonIntent1) buttonViewById1.setText((CharSequence) buttonIntent1.toString());
        if(null != buttonIntent2) buttonViewById2.setText((CharSequence) buttonIntent2.toString());
    }

    @Override
    protected void onStart() {
        Log.v(TAG, "onStart");
        super.onStart();
    }

    @Override
    protected void onRestart() {
        Log.v(TAG, "onRestart");
        super.onRestart();
    }

    @Override
    protected void onResume() {
        Log.v(TAG, "onResume");
        super.onResume();
    }

    @Override
    protected void onPause() {
        Log.v(TAG, "onPause");
        super.onPause();
    }

    @Override
    protected void onStop() {
        Log.v(TAG, "onStop");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.v(TAG, "onDestroy");
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_1, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setTAG(String TAG) {
        this.TAG = TAG;
    }

    public void buttonClicked1(View view) {
        Log.v(TAG, "clicked");
        if(null != buttonIntent1) startActivity(buttonIntent1);
    }

    public void buttonClicked2(View view) {
        Log.v(TAG, "clicked");
        if(null != buttonIntent2) startActivity(buttonIntent2);
    }

    public void setButtonIntent1(Intent buttonIntent1) {
        this.buttonIntent1 = buttonIntent1;
    }

    public void setButtonIntent2(Intent buttonIntent2) {
        this.buttonIntent2 = buttonIntent2;
    }
}
