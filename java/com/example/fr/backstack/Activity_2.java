package com.example.fr.backstack;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class Activity_2 extends LoggingActivity {

    private String TAG = getClass().getName() + "(" + serialNo + ")";

    private static int serialNo = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.setTAG(TAG);
        super.setButtonIntent1(new Intent(this, Activity_3.class)); //next
        super.setButtonIntent2(new Intent(this, this.getClass()));  //self
        super.onCreate(savedInstanceState);
        ++serialNo;
    }
}
